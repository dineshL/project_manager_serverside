package org.sep.agile.exceptions;

import org.hibernate.JDBCException;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class DatabaseException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5360798905892749162L;
	
	public DatabaseException(String message) {
		super(message);
	}
	
	public DatabaseException(JDBCException e) {
		super(ErrorMessageManager.getSqlErrorMessage(e));
	}

}
