package org.sep.agile.exceptions;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class DataNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6382154392003068639L;

	public DataNotFoundException(String message) {
		super(message);
	}
}
