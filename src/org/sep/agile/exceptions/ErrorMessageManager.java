package org.sep.agile.exceptions;

import org.hibernate.JDBCException;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ErrorMessageManager {

	public static String getSqlErrorMessage(JDBCException e) {
		return e.getSQLException().getLocalizedMessage().toString();
	}
	
}
