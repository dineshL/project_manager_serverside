package org.sep.agile.exceptions;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class AuthenticationException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4209533933645699385L;

	
	public AuthenticationException(String message) {
		super(message);
	}
}
