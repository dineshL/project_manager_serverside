package org.sep.agile.exceptions.mappers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.sep.agile.exceptions.DataNotFoundException;
import org.sep.agile.model.dto.Message;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Provider
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException>{

	@Override
	public Response toResponse(DataNotFoundException exception) {
		Message message = new Message();
		message.setCode(250);
		message.setMessage(exception.getMessage());
		System.out.println("Data not found");
		return Response.status(Status.NOT_FOUND)
				.entity(message)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}

}
