package org.sep.agile.exceptions.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.sep.agile.model.dto.Message;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable exception) {
		return Response
				.status(Status.FORBIDDEN)
				.entity(new Message(Status.FORBIDDEN.getStatusCode(), "There were some issues in the server or it may be down.please try again after some time period"))
				.build();
	}
	

}
