package org.sep.agile.exceptions.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.sep.agile.exceptions.DatabaseException;
import org.sep.agile.model.dto.Message;


/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Provider
public class DatabaseExceptionMapper implements ExceptionMapper<DatabaseException>{

	@Override
	public Response toResponse(DatabaseException exception) {
		Message message = new Message(500, exception.getMessage());
		System.out.println("My sql error mapper");
		return Response.status(Status.BAD_REQUEST)
				.entity(message)
				.build();
	}

}
