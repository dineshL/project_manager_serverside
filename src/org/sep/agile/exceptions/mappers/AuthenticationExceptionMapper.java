package org.sep.agile.exceptions.mappers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.sep.agile.exceptions.AuthenticationException;
import org.sep.agile.model.dto.Message;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Provider
public class AuthenticationExceptionMapper implements ExceptionMapper<AuthenticationException>{

	@Override
	public Response toResponse(AuthenticationException exception) {
		return Response
				.status(Status.FORBIDDEN)
				.entity(new Message(403, exception.getMessage()))
				.type(MediaType.APPLICATION_JSON)
				.build();
	}

}
