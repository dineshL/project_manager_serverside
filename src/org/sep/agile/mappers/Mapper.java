package org.sep.agile.mappers;

import org.sep.agile.enumarations.MappingLevel;

public abstract class Mapper<S, D> {
	
	public abstract D toDto(S model, MappingLevel level);
	
	public abstract S toModel(D dto);

}
