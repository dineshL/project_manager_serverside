package org.sep.agile.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.sep.agile.enumarations.MappingLevel;
import org.sep.agile.model.dto.ClientDTO;
import org.sep.agile.model.dto.DeveloperDTO;
import org.sep.agile.model.dto.ProductOwnerDTO;
import org.sep.agile.model.dto.UserDTO;
import org.sep.agile.model.hibernate.Client;
import org.sep.agile.model.hibernate.Developer;
import org.sep.agile.model.hibernate.ProductOwner;
import org.sep.agile.model.hibernate.User;

public class UserMapper extends Mapper<User, UserDTO>{

	@Override
	public UserDTO toDto(User model, MappingLevel level) {
		UserDTO dto = null;

		ModelMapper mapper = new ModelMapper();

		try {
			if(model instanceof Developer) {
				((Developer) model).setMembers(null);
				dto = mapper.map(model, DeveloperDTO.class);
				dto.setPassword(null);
				//((DeveloperDTO) dto).setProject(new Link("project", ((Developer) model).getProject().getId()));
			
			}else if(model instanceof Client) {
				dto = mapper.map(model, ClientDTO.class);
				dto.setPassword(null);
				
			}else if(model instanceof ProductOwner) {
				dto = mapper.map(model, ProductOwnerDTO.class);
				dto.setPassword(null);
				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		return dto;
	}

	@Override
	public User toModel(UserDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}


	/************************************************************
	 * 
	 ************************************************************/

	class LevelOne extends PropertyMap<User, UserDTO> {

		@Override
		protected void configure() {
			skip(destination.getEmail());
			skip(destination.getPassword());
			
		}

	}

	class LevelTwo extends LevelOne {
		@Override
		protected void configure() {
			super.configure();
			
		}
	}
}
