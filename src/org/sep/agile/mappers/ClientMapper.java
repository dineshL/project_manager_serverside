package org.sep.agile.mappers;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.sep.agile.model.dto.ClientDTO;
import org.sep.agile.model.hibernate.Client;

public class ClientMapper {

	public static ClientDTO map(Client client, boolean basic) {
		ModelMapper mapper = new ModelMapper();

		mapper.addMappings(new ClientMapper().new BasicMappingConf());

		return mapper.map(client, ClientDTO.class);
	}

	public static Client map(ClientDTO dto) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(dto, Client.class);
	}

	class BasicMappingConf extends PropertyMap<Client, ClientDTO> {

		@Override
		protected void configure() {
			
		}
	}

}
