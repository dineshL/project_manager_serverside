package org.sep.agile.mappers;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
import org.modelmapper.ModelMapper;
import org.sep.agile.mappers.conf.BasicUserDetails;
import org.sep.agile.mappers.conf.FullUserDetails;
import org.sep.agile.model.dto.UserDTO;
import org.sep.agile.model.hibernate.User;

public class MappingAgent {
	
	
	public MappingAgent() {
	}
	
	public static UserDTO toUserDTOBasic(User user) {
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new BasicUserDetails());
		
		return mapper.map(user, UserDTO.class);
	}
	
	public static UserDTO toUserDTOFull(User user) {
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new FullUserDetails());
		
		return mapper.map(user, UserDTO.class);
	}
	
	public static User toModelUser(UserDTO dto) {
		ModelMapper mapper = new ModelMapper();
		mapper.addMappings(new FullUserDetails());
		
		User map = mapper.map(dto, User.class);
		map.setState("1");
		return map;
	}
	
}
