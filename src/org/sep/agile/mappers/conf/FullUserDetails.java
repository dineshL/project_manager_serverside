package org.sep.agile.mappers.conf;

import org.modelmapper.PropertyMap;
import org.sep.agile.model.dto.UserDTO;
import org.sep.agile.model.hibernate.User;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class FullUserDetails extends PropertyMap<User, UserDTO>{
	
	@Override
	protected void configure() {
		
	}
}
