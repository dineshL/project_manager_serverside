package org.sep.agile.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.sep.agile.data.dao.ClientDAO;
import org.sep.agile.data.dao.UserDAO;
import org.sep.agile.model.dto.Link;
import org.sep.agile.model.dto.ProjectDTO;
import org.sep.agile.model.hibernate.Project;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ProjectMapper {

	/**
	 * Map project object to ProjectDTO object
	 * @param model
	 * @param basic true if Convert to basic model, else Full model
	 * @return
	 */
	public static ProjectDTO map(Project model, boolean basic) {
		ModelMapper mapper = new ModelMapper();
		model.setClient(null);
		mapper.addMappings(new ProjectMapper().new BasicMappingConf());
		ProjectDTO map = null;
		
		try {
			map = mapper.map(model, ProjectDTO.class);
			map.setScrumMaster(new Link("client", model.getScrumMaster().getId()));
			map.setProductOwner(new Link("productOwner", model.getProductOwner().getId()));
			//map.setClient(new Link("client", model.getClient().getId()));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public static Project map(ProjectDTO dto) {
		ModelMapper mapper = new ModelMapper();
		UserDAO userDao = new UserDAO();
		ClientDAO clientDAO = new ClientDAO();

		Project project = mapper.map(dto, Project.class);
		project.setProductOwner(userDao.getProductOwner(dto.getProductOwner().getId()));
		//project.setClient(clientDAO.findByEmail(dto.getClient()));

		return project;
	}



	class BasicMappingConf extends PropertyMap<Project, ProjectDTO> {

		@Override
		protected void configure() {
			skip(destination.getClient());
			skip(destination.getProductOwner());
			skip(destination.getScrumMaster());
			
		}
	}

}


