package org.sep.agile.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.sep.agile.enumarations.MappingLevel;
import org.sep.agile.model.dto.Link;
import org.sep.agile.model.dto.UserStoryDTO;
import org.sep.agile.model.hibernate.UserStory;
import org.sep.agile.util.DateUtil;

public class UserStoryMapper extends Mapper<UserStory, UserStoryDTO>{

	@Override
	public UserStoryDTO toDto(UserStory model, MappingLevel level) {
		ModelMapper mapper = new ModelMapper();
		UserStoryDTO map = new UserStoryDTO();
		switch (level) {
		case ONE: {
			mapper.addMappings(this.new LevelOne());
			map = mapper.map(model, UserStoryDTO.class);
			map.setEndDate(DateUtil.toString(model.getEndDate()));
			map.setStartingDate(DateUtil.toString(model.getStartingDate()));

			map.setProject(new Link("project", model.getProject().getId()));
			map.setSprint(new Link("sprint", model.getSprint().getId()));

			break;
		}
		case TWO: {
			mapper.addMappings(this.new LevelTwo());
			map = mapper.map(model, UserStoryDTO.class);
			
			break;
		}
		default:
			break;
		}
		
		return map;
	}

	@Override
	public UserStory toModel(UserStoryDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}


	/**************************************************************
	 *                                                            *
	 **************************************************************/

	class LevelOne extends PropertyMap<UserStory, UserStoryDTO>{

		@Override
		protected void configure() {
			skip(destination.getEndDate());
			skip(destination.getStartingDate());
			skip(destination.getProject());
			skip(destination.getSprint());
		}

	}

	class LevelTwo extends PropertyMap<UserStory, UserStoryDTO>{

		@Override
		protected void configure() {
			skip(destination.getEndDate());
			skip(destination.getStartingDate());
			skip(destination.getProject());
			skip(destination.getSprint());
		}

	}
}
