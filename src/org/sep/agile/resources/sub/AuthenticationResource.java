package org.sep.agile.resources.sub;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.sep.agile.model.dto.UserCredential;
import org.sep.agile.service.UserService;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationResource {
	
	@POST
	@Path("/login")
	public Response logIn(UserCredential credential) {
		UserService service = new UserService();
		return Response.ok(service.login(credential)).build();
		
	}
	
}
