package org.sep.agile.resources.sub;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.sep.agile.service.UserStoryService;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserStoryResource {
	
	@GET
	public Response getUserStories(@PathParam("projectId") long projectId) {
		UserStoryService service = new UserStoryService();
		
		return Response.ok(service.getAllUserStoriesOf(projectId)).build();
	}
	
	@GET
	@Path("/{id}")
	public Response getUserStoryOf(@PathParam("id") long id) {
		UserStoryService service = new UserStoryService();
		
		return Response.ok(service.getUserStory(id)).build();
	}
}
