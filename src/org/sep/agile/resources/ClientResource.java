package org.sep.agile.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.sep.agile.data.dao.ProjectDAO;
import org.sep.agile.data.dao.UserStoryDAO;
import org.sep.agile.mappers.ProjectMapper;
import org.sep.agile.model.dto.ClientDTO;
import org.sep.agile.model.dto.Message;
import org.sep.agile.model.dto.ProjectDTO;
import org.sep.agile.model.hibernate.Project;
import org.sep.agile.service.ClientService;

@Path("/clients")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClientResource {

	@GET
	public Response getAllClients(@QueryParam("get") String get) {
		ClientService service = new ClientService();
		
		if(get == null) {
			return Response.ok(service.getAllClients()).build();
		}
		
		if(get.equals("email")) {
			return Response.ok(service.getEmailList()).build();
		}
		
		return Response.status(Status.BAD_REQUEST).build();
	}
	
	@GET
	@Path("/{clientId}")
	public Response getClientById(@PathParam("clientId") Long clientId) {
		ClientService service = new ClientService();
		
		ClientDTO client = service.findById(clientId);
		
		return Response.ok(client).build();
	}
	
	@POST
	public Response addNewClient(ClientDTO dto) {
		ClientService service = new ClientService();
		
		Long clientId = service.insertClient(dto);
		
		if(clientId < 0) {
			return Response
					.status(Status.BAD_REQUEST)
					.entity(new Message(Status.BAD_REQUEST.getStatusCode(), "The remote server had some problems while saving the client.please check the details again and retry."))
					.build();
		}else {
			return Response.status(Status.CREATED).build();
		}
	}
	
	@Path("/test")
	@GET
	public Response test() {
		
		UserStoryDAO dao = new UserStoryDAO();
		ProjectDAO proDao = new ProjectDAO();
		/*for(int x = 0; x < 10; x++) {
			UserStory s = new UserStory();
			s.setDescription("description " + x);
			s.setDuration("d " + x);
			s.setEndDate(new Date());
			s.setProject(proDao.get(1));
			s.setStartingDate(new Date());
			
			dao.create(s);
		}*/
		Project project = proDao.get(1);
		ProjectDTO map = ProjectMapper.map(project, true);
		return Response.ok(map).build();
	}
}
