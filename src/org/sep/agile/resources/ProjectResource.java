
package org.sep.agile.resources;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.sep.agile.model.dto.Message;
import org.sep.agile.model.dto.ProjectDTO;
import org.sep.agile.resources.sub.UserStoryResource;
import org.sep.agile.service.ProjectService;

@Path("/projects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectResource {

	private ProjectService service;
	@Context UriInfo uriInfo;
	
	public ProjectResource() {
		service = new ProjectService();
	}
	
	@GET
	public Response getAllProjectes() {
		return Response.ok(service.getProjectList()).build();
	}
	
	@GET
	@Path("/{projectId}")
	public Response getProject(@PathParam("projectId") long projectId) {
		ProjectService service = new ProjectService();
		return Response.ok(service.getProject(projectId)).build();
	}

	@POST
	public Response addNewProject(ProjectDTO dto) {
		long projectId = service.addNewProject(dto);
		String uri = getUri("/" + String.valueOf(projectId)).toString();
		
		return Response.status(Status.CREATED)
				.entity(new Message(Status.CREATED.getStatusCode(),uri)).build();
	}
	
	@PUT
	@Path("/{projectId}")
	public Response updateProject(ProjectDTO dto) {
		return Response.ok(dto).build();
	}
	
	@DELETE
	@Path("/projectId")
	public Response deleteProject() {
		return Response.ok().build();
	}
	
	
	/***************************************************
	 *       Sub resources
	 **************************************************/
	
	@Path("{projectId}/userstories")
	public UserStoryResource getUserStoryResource() {
		return new UserStoryResource();
	}
	
	
	/*****************************************************
	 * 				Custom methods
	 ****************************************************/
	
	public URI getUri(String args) {
	  return uriInfo.getBaseUriBuilder().path(ProjectResource.class).path(args).build();
	}
}
