package org.sep.agile.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.sep.agile.mappers.MappingAgent;
import org.sep.agile.model.dto.Message;
import org.sep.agile.model.dto.UserDTO;
import org.sep.agile.resources.sub.AuthenticationResource;
import org.sep.agile.service.UserService;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

	/**
	 * 
	 * @return Response containing List of User objects
	 */
	@GET
	public Response getUsers(@QueryParam("username") String username) {
		UserService service = new UserService();
		if(username.trim().length() > 0) {
			UserDTO dto = service.getUserHaving(username);
			return Response.ok(dto).build();
		}
		
		return Response.ok("dinesh").build();
	}
	
	/**
	 * Filter user by the user name
	 * @param userId Long type variable
	 * @return User obejct of the requested user object
	 * user can be scrum master, Developer, Client or a Puruduct owner
	 */
	@GET
	@Path("/{userId}")
	public Response getUserById(@PathParam("userId") long userId) {
		UserService service = new UserService();
		
		return Response.ok(service.getUserOf(userId)).build();
	}
	
	/**
	 * Insert a new user to the database
	 * @param UserDTO object
	 * @return Response Object that will contain the basic information of the inserted user
	 */
	@POST
	public Response addNewUser(UserDTO dto) {
		UserService service = new UserService();
		
		UserDTO user = service.addNewUser(MappingAgent.toModelUser(dto));
		return Response.ok(user).build();
	}
	
	@PUT
	@Path("/{username}")
	public Response updateUser(UserDTO dto) {

		UserService service = new UserService();
		boolean updateUser = service.UpdateUser(dto);
		
		if(updateUser) return Response.ok().build();
		return Response.status(Status.BAD_REQUEST).build();
	}
	
	@GET
	@Path("/activate")
	public Response activateUser(@QueryParam("code") String code) {
		UserService service = new UserService();
		
		ResponseBuilder responseBuilder = Response.status(Status.BAD_REQUEST);
		Long activateUserId = service.activateUser(code);
		if(activateUserId != -1) {
			responseBuilder.entity(new Message(Status.OK.getStatusCode(), String.valueOf(activateUserId)))
							.status(Status.OK);
		}else {
			responseBuilder.entity(new Message(Status.BAD_REQUEST.getStatusCode(), "Invalid code, could not activate user"));
		}
		
		return responseBuilder.build();
	}
	
	
	/******************************************
	 * 		Sub resources					  *
	 ******************************************/
	
	@Path("/authenticate")
	public AuthenticationResource getAuthenticationResource() {
		return new AuthenticationResource();
	}
	
}
