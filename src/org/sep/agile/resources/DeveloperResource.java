package org.sep.agile.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.sep.agile.service.DeveloperService;

@Path("dev")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DeveloperResource {

	@GET
	@Path("/{devId}")
	public Response getDeveloper(@PathParam("devId") long devId) {
		DeveloperService service = new DeveloperService();
		
		return Response.ok(service.getDeveloper(devId)).build();
	}
	
}
