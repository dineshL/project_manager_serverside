package org.sep.agile.data;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ConnectionManager {

	private static SessionFactory sessionFactory = null;
	
	private ConnectionManager() {
		
	}
	
	public static SessionFactory getSessionFactory() {
		if(sessionFactory == null || sessionFactory.isClosed()) {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static boolean shutDown() {
		boolean result = false;
		try {	
			sessionFactory.close();
			result = true;
		}catch(Exception e) {
			result = false;
		}
		return result;
	}

}
