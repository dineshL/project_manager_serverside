package org.sep.agile.data.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.sep.agile.data.ConnectionManager;
import org.sep.agile.exceptions.DatabaseException;
import org.sep.agile.model.hibernate.Project;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ProjectDAO extends DaoImplAbstract<Project>{

	private SessionFactory factory;

	public ProjectDAO() {
		factory = ConnectionManager.getSessionFactory();
	}

	/**
	 * Insert a new project to the database
	 * @param dto
	 * @return
	 */
	@Override
	public Project create(Project project) {
		Session session = getSession();
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			if(project.getScrumMaster().getEmail() == null) project.setScrumMaster(null);
			session.save(project);
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {	
			throw new DatabaseException(e);
		
		}finally {
			session.close();
			
		}
		return project;
	}
	
	/**
	 * Get all the projects without filtering
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Project> getProjects() {
		Session session = null;
		List<Project> projects = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			projects = session.createCriteria(Project.class)
					.list();
			
			for(Project p : projects) {
				Hibernate.initialize(p.getScrumMaster());
				Hibernate.initialize(p.getClient());
				Hibernate.initialize(p.getProductOwner());
			}
			
			session.getTransaction().commit();
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			session.close();
			
		}
		
		return projects;
	}
	
	/**
	 * Get project list filtered using the project owner
	 * @param ownerId long
	 * @return List of projects
	 */
	@SuppressWarnings("unchecked")
	public List<Project> getProjects(long ownerId) {
		Session session = null;
		List<Project> projects = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			projects = session.createCriteria(Project.class, "pj")
					.createAlias("pj.productOwner", "o")
					.add(Restrictions.eq("id", ownerId))
					.list();
			
			session.getTransaction().commit();
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			session.close();
			
		}
		
		return projects;
	}
	
	
	/**
	 * Get a project using the peoject id
	 * @param projectId
	 * @return
	 */
	@Override
	public Project get(long id) {
		Session session = getSession();
		Project project = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			project = (Project) session.get(Project.class, id);
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			session.close();
			
		}
		return project;
	}
	
	@Override
	public Project update(Project project) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(Project project) {
		// TODO Auto-generated method stub
		return false;
	}

}
