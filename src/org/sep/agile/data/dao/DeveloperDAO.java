package org.sep.agile.data.dao;

import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.sep.agile.exceptions.DatabaseException;
import org.sep.agile.model.hibernate.Developer;

public class DeveloperDAO extends DaoImplAbstract<Developer>{

	@Override
	public Developer create(Developer s) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Developer update(Developer s) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(Developer s) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Developer get(long id) {
		Session session = getSession();
		Developer dev = null;
		
		try {
			session.beginTransaction();
			dev = (Developer) session.get(Developer.class, id);
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			closeSession(session);
		}
		
		return dev;
	}

}
