package org.sep.agile.data.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sep.agile.data.ConnectionManager;
import org.sep.agile.exceptions.DatabaseException;

public interface DAO<S>{
	
	public S create(S s);
	public S update(S s);
	public boolean delete(S s);
	public S get(long id);
	
}

abstract class DaoImplAbstract<T> implements DAO<T>{
	
	private SessionFactory factory;

	public DaoImplAbstract() {
		factory = ConnectionManager.getSessionFactory();
	}
	
	protected Session getSession() {
		Session session = null;
		try {
			session = factory.openSession();
		}catch(HibernateException e) {
			throw new DatabaseException(e.getLocalizedMessage().toString());
		}
		
		return session;
	}
	
	protected void closeSession(Session session) {
		session.close();
	} 
}