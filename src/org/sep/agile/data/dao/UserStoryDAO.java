package org.sep.agile.data.dao;

import java.util.List;

import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.sep.agile.exceptions.DatabaseException;
import org.sep.agile.model.hibernate.UserStory;

public class UserStoryDAO extends DaoImplAbstract<UserStory> {

	@Override
	public UserStory create(UserStory userStory) {
		Session session = getSession();
		
		try {
			session.beginTransaction();
			session.save(userStory);
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			closeSession(session);
			
		}
		return userStory;
	}

	@Override
	public UserStory update(UserStory userStory) {
		Session session = getSession();
		
		try {
			session.beginTransaction();
			session.update(userStory);
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			closeSession(session);
			
		}
		
		return userStory;
	}

	@Override
	public boolean delete(UserStory userStory) {
		Session session = getSession();
		boolean isSuccess = false;
		
		try {
			session.beginTransaction();
			session.delete(userStory);
			session.getTransaction().commit();
			isSuccess = true;
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			closeSession(session);
			
		}
		
		return isSuccess;
	}

	@Override
	public UserStory get(long id) {
		UserStory story = null;
		Session session = getSession();
		
		try {
			session.beginTransaction();
			story = (UserStory) session.get(UserStory.class, id);
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			closeSession(session);
			
		}
		
		return story;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserStory> getUserStoriesOf(long projectId) {
		Session session = getSession();
		List<UserStory> list = null;
		
		try {
			session.beginTransaction();
			list = session.createCriteria(UserStory.class, "story")
					.createAlias("story.project", "project")
					.add(Restrictions.eq("project.id", projectId))
					.list();
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			closeSession(session);
		}
		
		return list;
	}
}
