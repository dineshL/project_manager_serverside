package org.sep.agile.data.dao;

import org.hibernate.Hibernate;
import org.hibernate.JDBCException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.sep.agile.data.ConnectionManager;
import org.sep.agile.exceptions.DatabaseException;
import org.sep.agile.model.hibernate.ProductOwner;
import org.sep.agile.model.hibernate.User;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class UserDAO {

	private SessionFactory factory;

	public UserDAO() {
		factory = ConnectionManager.getSessionFactory();
	}

	/**
	 * get a particular user using the userId
	 * @param userId @long
	 * @return @User
	 */
	public User getUser(long userId) {
		User user = null;
		Session session = null;
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			user = (User) session.get(User.class, userId);
			if(user instanceof ProductOwner) Hibernate.initialize(((ProductOwner) user).getProjects());
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			e.printStackTrace();
			
		}finally {
			session.close();
		}
		
		return user;
	}
	
	/**
	 * get a particular user using the username
	 * @param username @String
	 * @return @User
	 */
	public User getUser(String username) {
		User user = null;
		Session session = null;
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			user = (User) session.createCriteria(User.class)
					.add(Restrictions.eq("username", username))
					.uniqueResult();
			if(user instanceof ProductOwner) Hibernate.initialize(((ProductOwner) user).getProjects());
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			e.printStackTrace();
			
		}finally {
			session.close();
		}
		
		return user;
	}
	
	
	/**
	 * Insert a new user to the database
	 * @param user
	 * @return User object with the generated userId
	 */
	public User insertUser(User user) {
		Session session = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			session.save(user);
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			
			session.close();
		}
		
		return user;
	}
	
	/**
	 * Update a particular user
	 * @param user
	 * @return boolean indicating update status
	 */
	public boolean updateUser(User user) {
		Session session = null; 
		boolean result = false;
		
		try {
			session = factory.openSession();
			session.beginTransaction();

			session.update(user);
			
			session.getTransaction().commit();
			result = true;
			
		}catch(JDBCException e) {
			result = false;
			e.printStackTrace();
		}finally {
			session.close();
		}
		
		return result;
	}
	
	public boolean isUserAvailable(String username) {
		Session session = null;
		long count = -1;
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			count = (long) session.createCriteria(User.class)
					.add(Restrictions.eq("username", username))
					.setProjection(Projections.rowCount())
					.uniqueResult();
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			e.printStackTrace();
			count = -1;
			
		}finally {
			session.close();
		}
		
		return count <= 0 ? false : true;
	}
	
	public Long activate(String code) {
		Session session = null;
		Long userId = -1L;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			/*Query queryGetId = session.createSQLQuery("SELECT `user_id` FROM `user` WHERE `state` = :code");
			queryGetId.setParameter("code", code);
			
			List<Long> list = queryGetId.list();
			userId = list.get(0);*/
			
			userId = (Long) session.createCriteria(User.class)
					.add(Restrictions.eq("state", code))
					.setProjection(Projections.property("id"))
					.uniqueResult();
			
			Query query = session.createSQLQuery("UPDATE `user` SET `state` = '1' WHERE `state` = :code");
			query.setParameter("code", code);
			
			int num = query.executeUpdate();
			System.out.println(num);
			if(num > 0) {
				session.getTransaction().commit();
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			//throw new DatabaseException(e);
		}finally {
			
			session.close();
		}
		
		return userId;
	}
	
	public ProductOwner getProductOwner(Long id) {
		Session session = null;
		ProductOwner po = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			po = (ProductOwner) session.get(ProductOwner.class, id);
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			session.close();
			
		}
		
		return po;
	}
}
