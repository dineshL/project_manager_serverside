package org.sep.agile.data.dao;

import java.util.List;

import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.sep.agile.data.ConnectionManager;
import org.sep.agile.exceptions.DatabaseException;
import org.sep.agile.model.hibernate.Client;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ClientDAO {

	private SessionFactory factory;

	public ClientDAO() {
		factory = ConnectionManager.getSessionFactory();
	}
	
	public Client insertClient(Client client) {
		Session session = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			session.save(client);
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			session.close();
		}
		
		return client;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Client> getClients() {
		Session session = null;
		List<Client> clients = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			clients = session.createCriteria(Client.class)
					.list();
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		
		return clients;
	}

	
	@SuppressWarnings("unchecked")
	public List<String> getEmailList() {
		Session session = null;
		List<String> emails = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			emails = session.createCriteria(Client.class)
					.setProjection(Projections.property("email"))
					.list();
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			session.close();
			
		}
		
		return emails;
	}
	
	public Client findByEmail(String email) {
		Session session = null;
		Client client = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			client = (Client) session.createCriteria(Client.class)
					.add(Restrictions.eq("email", email))
					.uniqueResult();
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			session.close();
			
		}
		
		return client;
	}
	
	public Client findById(long id) {
		Session session = null;
		Client client = null;
		
		try {
			session = factory.openSession();
			session.beginTransaction();
			
			client = session.get(Client.class, id);
			
			session.getTransaction().commit();
			
		}catch(JDBCException e) {
			throw new DatabaseException(e);
			
		}finally {
			
			session.close();
		}
		
		return client;
	}
}
