package org.sep.agile.service;

import java.util.List;

import org.sep.agile.data.dao.UserStoryDAO;
import org.sep.agile.enumarations.MappingLevel;
import org.sep.agile.exceptions.DataNotFoundException;
import org.sep.agile.mappers.UserStoryMapper;
import org.sep.agile.model.dto.UserStoryDTO;
import org.sep.agile.model.dto.lists.UserStoryList;
import org.sep.agile.model.hibernate.UserStory;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

public class UserStoryService {

	public UserStoryDTO getUserStory(long id) {
		UserStoryDAO dao = new UserStoryDAO();
		UserStory model = dao.get(id);
		
		if(model == null) throw new DataNotFoundException("No user story available for id: " + id);
		
		return new UserStoryMapper().toDto(model, MappingLevel.ONE);
	}
	
	public UserStoryList getAllUserStoriesOf(long projectId) {
		UserStoryDAO dao = new UserStoryDAO();
		UserStoryList list = new UserStoryList();
		
		List<UserStory> modelList = dao.getUserStoriesOf(projectId);
		
		if(modelList.isEmpty()) throw new DataNotFoundException("No user stories found for project id: " + projectId);
		
		for(UserStory story : modelList) {
			list.getList().add(new UserStoryMapper().toDto(story, MappingLevel.TWO));
		}
		
		return list;
	}
}
