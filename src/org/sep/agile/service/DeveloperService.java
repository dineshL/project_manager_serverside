package org.sep.agile.service;

import org.sep.agile.data.dao.DeveloperDAO;
import org.sep.agile.enumarations.MappingLevel;
import org.sep.agile.exceptions.DataNotFoundException;
import org.sep.agile.mappers.UserMapper;
import org.sep.agile.model.dto.DeveloperDTO;
import org.sep.agile.model.hibernate.Developer;

public class DeveloperService {

	public DeveloperDTO getDeveloper(long developerId) {
		DeveloperDAO dao = new DeveloperDAO();
		
		Developer model = dao.get(developerId);
		
		if(model == null) throw new DataNotFoundException("no developer details found for");
		
		return (DeveloperDTO) new UserMapper().toDto(model, MappingLevel.ONE);
	}
	
}
