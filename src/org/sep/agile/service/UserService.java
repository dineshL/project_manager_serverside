package org.sep.agile.service;

import java.util.UUID;

import org.sep.agile.data.dao.DeveloperDAO;
import org.sep.agile.data.dao.UserDAO;
import org.sep.agile.emailing.EmailManager;
import org.sep.agile.enumarations.MappingLevel;
import org.sep.agile.exceptions.AuthenticationException;
import org.sep.agile.exceptions.DataNotFoundException;
import org.sep.agile.exceptions.DatabaseException;
import org.sep.agile.mappers.MappingAgent;
import org.sep.agile.mappers.UserMapper;
import org.sep.agile.model.dto.DeveloperDTO;
import org.sep.agile.model.dto.UserCredential;
import org.sep.agile.model.dto.UserDTO;
import org.sep.agile.model.hibernate.Developer;
import org.sep.agile.model.hibernate.User;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class UserService {

	/**
	 * Insert a new User to the database
	 * @param User Object
	 * @return UserDTO object
	 */
	public UserDTO addNewUser(User user) {
		UserDAO dao = new UserDAO();
		String uuid = genarateNextSessionId();
		user.setState(uuid);
		if(user.getRole() == null) user.setRole("Developer");
		User insertUser = dao.insertUser(user);

		if(insertUser == null) {
			throw new DatabaseException("There was some problem while inserting the data. please try again");
		}

		EmailManager emailManager = new EmailManager();
		emailManager.sendEmail(uuid, user.getEmail(), "user");

		return MappingAgent.toUserDTOBasic(insertUser);
	}

	/**
	 * Login service method
	 * @param UserCredential Object
	 * @return User Object
	 * null if the user is not authenticated
	 */
	public UserDTO login(UserCredential credential) {
		AuthenticationService authenticationService = new AuthenticationService();
		User user = authenticationService.autheticate(credential);

		if(user != null) {
			return MappingAgent.toUserDTOBasic(user);
		}

		return null;
	}

	public UserDTO getUserHaving(String username) {
		UserDAO dao = new UserDAO();

		User model = dao.getUser(username);

		if(model == null) {
			throw new DataNotFoundException("No user data avalable for provided username");
		}

		//converting the Hibernate User object into the UserDTO object and returning
		return new UserMapper().toDto(model, MappingLevel.ONE);
	}


	public boolean UpdateUser(UserDTO dto) {
		UserDAO dao = new UserDAO();
		dto.setPassword(dao.getUser(dto.getId()).getPassword());

		return dao.updateUser(MappingAgent.toModelUser(dto));

	}

	public Long activateUser(String code) {
		UserDAO dao = new UserDAO();
		return dao.activate(code);
	}

	public UserDTO getUserOf(long userId) {
		UserDAO dao = new UserDAO();

		User model = dao.getUser(userId);

		if(model == null) throw new DataNotFoundException("no developer details found for");

		return new UserMapper().toDto(model, MappingLevel.ONE);
	}


	/***************************************************************
	 * 
	 ***************************************************************/
	private String genarateNextSessionId() {
		return UUID.randomUUID().toString();
	}
}

/**
 * inner class
 * can be accessed only by authentication class
 * @author Dinesh Liyanage
 *
 */
class AuthenticationService {

	/**
	 * Used to authenticate a user to use the system
	 * 
	 * @param UserCredetial object
	 * @return User object
	 * null if the user is not authenticated
	 */
	public User autheticate(UserCredential credential) {
		UserDAO dao = new UserDAO();

		if(!dao.isUserAvailable(credential.getUsername())) {
			throw new AuthenticationException("incorrect username or password.");
		}else {
			User user = dao.getUser(credential.getUsername());
			if(user.getState().equals("1")) {
				if(user.getPassword().equals(credential.getPassword())) {
					return user;
				}else {
					throw new AuthenticationException("incorrect password or password");
				}
			}else {
				throw new AuthenticationException("Your account is not acctivated. please activate it using the email you received");
			}
		}
	}

}

