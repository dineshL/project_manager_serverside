package org.sep.agile.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.sep.agile.data.dao.ClientDAO;
import org.sep.agile.emailing.EmailManager;
import org.sep.agile.exceptions.DataNotFoundException;
import org.sep.agile.mappers.ClientMapper;
import org.sep.agile.model.dto.ClientDTO;
import org.sep.agile.model.dto.lists.ClientList;
import org.sep.agile.model.dto.lists.EmailList;
import org.sep.agile.model.hibernate.Client;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ClientService {

	public ClientList getAllClients() {
		ClientDAO dao = new ClientDAO();
		
		ClientList list = new ClientList();
		
		for(Client client : dao.getClients()) {
			list.getList().add(ClientMapper.map(client, true));
			System.out.println(list.getList().get(0).getOrganization());
		}
		
		if(list.getList().isEmpty()) {
			throw new DataNotFoundException("No clients found");
		}
		
		return list;
	}
	
	public EmailList getEmailList() {
		ClientDAO dao = new ClientDAO();
		
		List<String> emails = dao.getEmailList();
		
		if(emails.isEmpty()) {
			throw new DataNotFoundException("no emails found for clients");
		}
		
		EmailList list = new EmailList();
		emails.stream().forEach(e -> {
			list.getList().add(e);
		});
		
		return list;
	}
	
	
	public Long insertClient(ClientDTO dto) {
		ClientDAO dao = new ClientDAO();
		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String tempUp = dateFormat.format(date).toString();
		dto.setPassword("p" + tempUp);
		dto.setRole("Client");
		dto.setUsername("u" + tempUp);
		
		Client client = ClientMapper.map(dto);
		String uuid = genarateNextSessionId();
		client.setState(uuid);
		
		EmailManager emailManager = new EmailManager();
		emailManager.sendEmail(uuid, client.getEmail(), "client");

		
		return dao.insertClient(client).getId();
	}
	
	public ClientDTO findById(Long id) {
		ClientDAO dao = new ClientDAO();
		Client client = dao.findById(id);
		
		ClientDTO dto = ClientMapper.map(client, true);
		
		if(dto == null) {
			
			throw new DataNotFoundException("Some error occured while loding the client. please try again");
		}
		
		return dto;
	}
	
	private String genarateNextSessionId() {
		return UUID.randomUUID().toString();
	}
}
