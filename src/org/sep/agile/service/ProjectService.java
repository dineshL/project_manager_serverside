package org.sep.agile.service;

import org.sep.agile.data.dao.ProjectDAO;
import org.sep.agile.exceptions.DataNotFoundException;
import org.sep.agile.mappers.ProjectMapper;
import org.sep.agile.model.dto.ProjectDTO;
import org.sep.agile.model.dto.lists.ProjectList;
import org.sep.agile.model.hibernate.Project;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ProjectService {

	private ProjectDAO dao;

	public ProjectService() {
		dao = new ProjectDAO();
	}

	/**
	 * Controllers the insertion of a new project to the database
	 * @param dto ProjectDTO object
	 * @return id of the newly inserted project
	 */
	public long addNewProject(ProjectDTO dto) {
		Project project = ProjectMapper.map(dto);
		
		long projectId = dao.create(project).getId();

		if(projectId < 0) {
			throw new DataNotFoundException("Could not add the project");
		}

		return projectId;
	}


	/**
	 * get all the project list
	 * @return
	 */
	public ProjectList getProjectList() {
		ProjectList projectList = new ProjectList();

		for(Project p : dao.getProjects()) {
			projectList.getList().add(ProjectMapper.map(p, true));
		}

		//checking for empty list
		if(projectList.getList().isEmpty()) throw new DataNotFoundException("No project detail found");

		return projectList;
	}
	
	/**
	 * get projects filtered using the owner
	 * @return
	 */
	public ProjectList getProjectList(long ownerId) {
		ProjectList projectList = new ProjectList();

		for(Project p : dao.getProjects()) {
			projectList.getList().add(ProjectMapper.map(p, true));
		}

		//checking for empty list
		if(projectList.getList().isEmpty()) throw new DataNotFoundException("No project detail found");

		return projectList;
	}
	
	public ProjectDTO getProject(long projectId) {
		Project project = dao.get(projectId);
		
		if(project == null) throw new DataNotFoundException("No project detail found for id: " + projectId);
		
		ProjectDTO dto = ProjectMapper.map(project, false);
		
		return dto;
	}
	//test commit
	
}