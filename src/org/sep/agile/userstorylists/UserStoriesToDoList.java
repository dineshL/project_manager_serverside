package org.sep.agile.userstorylists;

import java.util.ArrayList;
import java.util.List;

import org.sep.agile.model.dto.UserStoryDTO;

public class UserStoriesToDoList {

	private List<UserStoryDTO> list = new ArrayList<UserStoryDTO>();
	
	
	public UserStoriesToDoList () {
		
	}


	public List<UserStoryDTO> getList() {
		return list;
	}


	public void setList(List<UserStoryDTO> list) {
		this.list = list;
	}
}
