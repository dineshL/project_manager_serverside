package org.sep.agile.interfaces;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public interface Addressable {

	public String getLink();
	
}
