package org.sep.agile.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static Date toDate(String dateInString) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		//String dateInString = "07/06/2013";
		Date date = null;
		try {
			date = formatter.parse(dateInString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}

	
	public static String toString(Date dateInDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		//String dateInString = "07/06/2013";
		String date = null;
		try {
			date = formatter.format(dateInDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return date;
	}
}
