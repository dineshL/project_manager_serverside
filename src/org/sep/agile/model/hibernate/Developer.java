package org.sep.agile.model.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "developer")
@PrimaryKeyJoinColumn(name = "user_id")
public class Developer extends User{
	
	private Developer scrumMaster;
	private List<Developer> members = new ArrayList<Developer>();
	private Project project;

	public Developer() {
		super();
	}
	
	@ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="scrum_master_id")
	public Developer getScrumMaster() {
		return scrumMaster;
	}

	@OneToMany(mappedBy="scrumMaster")
	public List<Developer> getMembers() {
		return members;
	}
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "scrumMaster", cascade = CascadeType.ALL)
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setScrumMaster(Developer scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	public void setMembers(List<Developer> members) {
		this.members = members;
	}
}
