package org.sep.agile.model.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "state", uniqueConstraints = {
		@UniqueConstraint(columnNames = "state_id")
})
public class State {

	private int id;
	private String description;
	
	public State() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "state_id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	@Column(name = "description", nullable = false, unique = true)
	public String getDescription() {
		return description;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
