package org.sep.agile.model.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "product_owner")
@PrimaryKeyJoinColumn(name = "user_id")
public class ProductOwner extends User{

	private List<Project> projects = new ArrayList<Project>();
	
	public ProductOwner() {
		
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "productOwner", cascade = CascadeType.PERSIST)
	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
	
}
