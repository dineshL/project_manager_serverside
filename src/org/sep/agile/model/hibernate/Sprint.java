package org.sep.agile.model.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "sprint", uniqueConstraints = {
		@UniqueConstraint(columnNames = "sprint_id")
})
public class Sprint {

	private long id;
	private Date from;
	private Date to;
	private Itaration itaration;
	private List<UserStory> userStories = new ArrayList<UserStory>();
	
	public Sprint() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "sprint_id", unique = true, nullable = false)
	public long getId() {
		return id;
	}
	
	@Column(name = "from", nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFrom() {
		return from;
	}

	@Column(name = "to", nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getTo() {
		return to;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sprint", cascade = CascadeType.PERSIST)
	public List<UserStory> getUserStories() {
		return userStories;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "itaration_id", nullable = false)
	public Itaration getItaration() {
		return itaration;
	}

	public void setItaration(Itaration itaration) {
		this.itaration = itaration;
	}

	public void setUserStories(List<UserStory> userStories) {
		this.userStories = userStories;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public void setTo(Date to) {
		this.to = to;
	}
	
	
}
