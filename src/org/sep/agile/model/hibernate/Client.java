package org.sep.agile.model.hibernate;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "client")
@PrimaryKeyJoinColumn(name = "user_id")
public class Client extends User{

	private String organization;
	private String contactNumber;
	
	public Client() {
		super();
	}

	public String getOrganization() {
		return organization;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	
}
