package org.sep.agile.model.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "itaration", uniqueConstraints = {
		@UniqueConstraint(columnNames = "itaration_id")
})
public class Itaration {

	private long id;
	private String description;
	private Date startingDate;
	private Date endDate;
	private Project project;
	private List<Sprint> sprints = new ArrayList<Sprint>();
	
	public Itaration() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "itaration_id", nullable = false, unique = true)
	public long getId() {
		return id;
	}

	@Column(name = "description", nullable = false, length = 150)
	public String getDescription() {
		return description;
	}

	@Column(name = "starting_date")
	@Temporal(TemporalType.DATE)
	public Date getStartingDate() {
		return startingDate;
	}

	@Column(name = "end_date")
	@Temporal(TemporalType.DATE)
	public Date getEndDate() {
		return endDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "itaration", cascade = CascadeType.PERSIST)
	public List<Sprint> getSprints() {
		return sprints;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_id", nullable = false)
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setSprints(List<Sprint> sprints) {
		this.sprints = sprints;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
}
