package org.sep.agile.model.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "project", uniqueConstraints = {
		@UniqueConstraint(columnNames = "project_id"),
		@UniqueConstraint(columnNames = "project_name")
})
public class Project {

	private long id;
	private String name;
	private List<UserStory> userStories = new ArrayList<UserStory>();
	private List<Itaration> itarations = new ArrayList<Itaration>();
	private ProductOwner productOwner;
	private Developer scrumMaster;
	private Client client;
	
	
	public Project() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "project_id", nullable = false, unique = true)
	public long getId() {
		return id;
	}

	@Column(name = "project_name", nullable = false, length = 150, unique = true)
	public String getName() {
		return name;
	}

	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL)
	public List<UserStory> getUserStories() {
		return userStories;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL)
	public List<Itaration> getItarations() {
		return itarations;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id", nullable = false)
	public ProductOwner getProductOwner() {
		return productOwner;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "scrum_master_id")
	public Developer getScrumMaster() {
		return scrumMaster;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "client_id", nullable = true)
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public void setScrumMaster(Developer scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	public void setProductOwner(ProductOwner productOwner) {
		this.productOwner = productOwner;
	}

	public void setItarations(List<Itaration> itarations) {
		this.itarations = itarations;
	}

	public void setUserStories(List<UserStory> userStories) {
		this.userStories = userStories;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
