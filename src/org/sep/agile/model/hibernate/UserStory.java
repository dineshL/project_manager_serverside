package org.sep.agile.model.hibernate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "user_stroy", uniqueConstraints = {
		@UniqueConstraint(columnNames = "story_id")
})
public class UserStory {

	private long id;
	private String description;
	private Date startingDate;
	private Date endDate;
	private String duration;
	private Project project;
	private Sprint sprint;
	private Integer index;

	
	public UserStory() {
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "story_id", nullable = false, unique = true)
	public long getId() {
		return id;
	}

	@Column(name = "decsription", nullable = false, length = 1000)
	public String getDescription() {
		return description;
	}

	@Column(name = "starting_date", nullable = true)
	@Temporal(TemporalType.DATE)
	public Date getStartingDate() {
		return startingDate;
	}

	@Column(name = "end_date", nullable = true)
	@Temporal(TemporalType.DATE)
	public Date getEndDate() {
		return endDate;
	}

	@Column(name = "duration", nullable = false, length = 3)
	public String getDuration() {
		return duration;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_id", nullable = false)
	public Project getProject() {
		return project;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sprint_id", nullable = true)
	public Sprint getSprint() {
		return sprint;
	}
	
	@Column(name = "user_story_index")
	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
