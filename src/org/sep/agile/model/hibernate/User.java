package org.sep.agile.model.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "user", catalog = "pmd", uniqueConstraints = {
		@UniqueConstraint(columnNames = "user_id")
})
@Inheritance(strategy = InheritanceType.JOINED)
public class User {

	private long id;
	private String name;
	private String email;
	private String password;
	private String username;
	private String role;
	private String state;
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@Column(name = "user_id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	@Column(name = "name", length = 50, nullable = false)
	public String getName() {
		return name;
	}

	@Column(name = "email", unique = true, length = 100, nullable = false)
	public String getEmail() {
		return email;
	}

	@Column(name = "password", length = 15, unique = false, nullable = false)
	public String getPassword() {
		return password;
	}

	@Column(name = "username", unique = true, nullable = false, length = 15)
	public String getUsername() {
		return username;
	}

	@Column(name = "role", nullable = true)
	public String getRole() {
		return role;
	}

	@Column(name = "state", nullable = false, length = 100)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
