package org.sep.agile.model.dto;

import java.util.ArrayList;
import java.util.List;

public class DeveloperDTO extends UserDTO{

	private Link scrumMaster;
	private List<Link> members = new ArrayList<Link>();
	private Link project;
	
	public DeveloperDTO() {
		// TODO Auto-generated constructor stub
	}

	public Link getScrumMaster() {
		return scrumMaster;
	}

	public List<Link> getMembers() {
		return members;
	}

	public Link getProject() {
		return project;
	}

	public void setScrumMaster(Link scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	public void setMembers(List<Link> members) {
		this.members = members;
	}

	public void setProject(Link project) {
		this.project = project;
	}
	
	
}
