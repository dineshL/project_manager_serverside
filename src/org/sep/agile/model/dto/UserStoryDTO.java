package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class UserStoryDTO {
	
	private long id;
	private String description;
	private String startingDate;
	private String endDate;
	private String duration;
	private Integer index;
	private Link project;
	private Link sprint;
	
	public UserStoryDTO() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getStartingDate() {
		return startingDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getDuration() {
		return duration;
	}

	public Link getProject() {
		return project;
	}

	public Link getSprint() {
		return sprint;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setProject(Link project) {
		this.project = project;
	}

	public void setSprint(Link sprint) {
		this.sprint = sprint;
	}

	
}
