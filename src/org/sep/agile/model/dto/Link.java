package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Link {

	private String rel;
	private Long id;
	
	public Link() {
		// TODO Auto-generated constructor stub
	}

	public Link(String rel, Long id) {
		super();
		this.rel = rel;
		this.id = id;
	}

	public String getRel() {
		return rel;
	}

	public Long getId() {
		return id;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	
}
