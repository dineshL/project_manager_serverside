package org.sep.agile.model.dto.lists;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.sep.agile.model.dto.ClientDTO;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class ClientList {

	List<ClientDTO> list = new ArrayList<ClientDTO>();
	
	public ClientList() {
		// TODO Auto-generated constructor stub
	}

	public List<ClientDTO> getList() {
		return list;
	}

	public void setList(List<ClientDTO> list) {
		this.list = list;
	}
	
}
