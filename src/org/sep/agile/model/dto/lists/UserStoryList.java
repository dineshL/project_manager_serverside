package org.sep.agile.model.dto.lists;

import java.util.ArrayList;
import java.util.List;

import org.sep.agile.model.dto.UserStoryDTO;


public class UserStoryList {

	private List<UserStoryDTO> list = new ArrayList<UserStoryDTO>();
	
	public UserStoryList() {
		// TODO Auto-generated constructor stub
	}

	public List<UserStoryDTO> getList() {
		return list;
	}

	public void setList(List<UserStoryDTO> list) {
		this.list = list;
	}
	
}
