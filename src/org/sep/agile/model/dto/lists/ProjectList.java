package org.sep.agile.model.dto.lists;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.sep.agile.model.dto.ProjectDTO;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class ProjectList {

	private List<ProjectDTO> list = new ArrayList<ProjectDTO>();
	
	public ProjectList() {
		// TODO Auto-generated constructor stub
	}

	public List<ProjectDTO> getList() {
		return list;
	}

	public void setList(List<ProjectDTO> list) {
		this.list = list;
	}
	
}
