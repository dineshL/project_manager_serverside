package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class Message {
	
	private String message;
	private int code;
	
	public Message() {
		// TODO Auto-generated constructor stub
	}


	public Message(int code, String message) {
		this.message = message;
		this.code = code;
	}



	public String getMessage() {
		return message;
	}

	public int getCode() {
		return code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
	
}
