package org.sep.agile.model.dto;

import java.util.ArrayList;
import java.util.List;

public class ProductOwnerDTO extends UserDTO {

	private List<Link> projects = new ArrayList<Link>();

	public ProductOwnerDTO() {}
	
	public List<Link> getProjects() {
		return projects;
	}

	public void setProjects(List<Link> projects) {
		this.projects = projects;
	}

}
