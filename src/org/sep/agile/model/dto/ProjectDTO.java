package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class ProjectDTO {

	private long id;
	private String name;
	private Link productOwner;
	private Link scrumMaster;
	private Link client;
	
	public ProjectDTO() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Link getProductOwner() {
		return productOwner;
	}

	public Link getScrumMaster() {
		return scrumMaster;
	}

	public Link getClient() {
		return client;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProductOwner(Link productOwner) {
		this.productOwner = productOwner;
	}

	public void setScrumMaster(Link scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	public void setClient(Link client) {
		this.client = client;
	}

}
