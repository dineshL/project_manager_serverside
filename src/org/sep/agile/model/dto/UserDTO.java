package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class UserDTO {
	
	private long id;
	private String username;
	private String name;
	private String password;
	private String email;
	private String role;
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public String getName() {
		return name;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}
		
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
