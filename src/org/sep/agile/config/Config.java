package org.sep.agile.config;

import org.glassfish.jersey.server.ResourceConfig;


/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class Config extends ResourceConfig {

	public Config() {
		packages("org.sep.agile.exceptions.mappers");
		packages("org.glassfish.jersey.examples.multipart");
		
		/*********Exception Mappers***************/
		
       /* register(AuthenticationExceptionMapper.class);
        register(ThrowableExceptionMapper.class);
        register(JsonMappingExceptionMapper.class);
        register(DatabaseExceptionMapper.class);*/

        
        
	}
}
