package org.sep.agile.emailing;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.sep.agile.exceptions.DataNotFoundException;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class EmailManager {

	public void sendEmail(String uuid, String to, String type) {
		final String username = "dinesh.pl.dl@gmail.com";
		final String password = "Dreshani";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,new GMailAuthenticator(username, password));

		try {
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("dinesh.pl.dl@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject("Agile project managemt system activation");
			message.setText("Dear user,"
				+ "\n\n No spam to my email, please!"
				+ "http://localhost:8080/agaile/email?verify&code=" + uuid + "&type=" + type);

			Transport.send(message);

			System.out.println("email sent");

		} catch (Exception e) {
			
			throw new DataNotFoundException(e.getMessage());
		}
	}
}


class GMailAuthenticator extends Authenticator {
    String user;
    String pw;
    public GMailAuthenticator (String username, String password)
    {
       super();
       this.user = username;
       this.pw = password;
    }
   public PasswordAuthentication getPasswordAuthentication()
   {
      return new PasswordAuthentication(user, pw);
   }
}
